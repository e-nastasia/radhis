# Zome Developer Setup

This folder has an example DNA for the `radhis` zome. The actual code for the zome is in `zomes/radhis`.

All the instructions here assume you are running them inside the nix-shell at the root of the repository. For more info, see the [developer setup](/dev-setup.md).

** NOTE:** Testing is not working yet. The zome can be compiled, but that is all. The below instructions will be useful when the project has advanced more!


## Building

```bash
CARGO_TARGET=target cargo build --release --target wasm32-unknown-unknown
hc dna pack file_storage.dna.workdir
```

This should create a `radhis.dna.workdir/radhis-test.dna` file.

## Testing

After having built the DNA:

```bash
cd test
npm install
npm test
```

## Running

After having built the DNA:

```bash
hc s call register-dna --path zome/radhis.dna.workdir/radhis-test.dna
hc s call install-app <RESULT_HASH_OF_PREVIOUS_COMMAND>
hc s run
```

Now `holochain` will be listening at port `8888`;
