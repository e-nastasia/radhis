use hdk::prelude::*;

mod radhis;
mod utils;

use radhis::HealthDataOutput;
use radhis::AuditDataOutput;

pub fn error<T>(reason: &str) -> ExternResult<T> {
    Err(WasmError::Guest(String::from(reason)))
}

entry_defs![
    Path::entry_def(),
    radhis::HealthData::entry_def(),
    radhis::AuditData::entry_def()
];

// create health_data
#[hdk_extern]
pub fn create_health_data(
    health_data_input: radhis::HealthDataInput,
) -> ExternResult<HealthDataOutput> {
    radhis::create_health_data(health_data_input)
}

// list all health_data entries
#[hdk_extern]
pub fn list_health_data(_: ()) -> ExternResult<Vec<HealthDataOutput>> {
    let health_data = radhis::list_health_data()?;

    Ok(health_data)
}

// create audit_data
#[hdk_extern]
pub fn create_audit_data(
    audit_data_input: radhis::AuditDataInput,
) -> ExternResult<AuditDataOutput> {
    radhis::create_audit_data(audit_data_input)
}

// get audit_data
#[hdk_extern]
pub fn get_audit_data(_: ()) -> ExternResult<Vec<AuditDataOutput>> {
    let audit_data = radhis::get_audit_data()?;

    Ok(audit_data)
}

// cap token stuff
#[hdk_extern]
fn init(_: ()) -> ExternResult<InitCallbackResult> {
    // grant unrestricted access to accept_cap_claim so other agents can send us claims
    let mut functions: GrantedFunctions = HashSet::new();
    functions.insert((zome_info()?.zome_name, "receive_cap_access".into()));
    create_cap_grant(CapGrantEntry {
        tag: "".into(),
        // empty access converts to unrestricted
        access: ().into(),
        functions,
    })?;

    Ok(InitCallbackResult::Pass)
}

#[hdk_extern]
pub fn get_cap_tokens(filter_by_agent: AgentPubKey) -> ExternResult<Vec<CapClaim>> {
    let cap_claims = query(ChainQueryFilter::new().entry_type(EntryType::CapClaim).include_entries(true))?
        .into_iter()
        .map(|elem| {
            elem.entry().to_owned().into_option().unwrap().as_cap_claim().expect("Could not get option").to_owned()
        })
        .filter(|grant| grant.grantor() == &filter_by_agent)
        .collect::<Vec<CapClaim>>();
    debug!("Got claims: {:#?}", cap_claims);
    Ok(cap_claims)
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct CapReceive {
    pub cap_secret: CapSecret,
    pub from_agent: AgentPubKey
}

#[hdk_extern]
pub fn receive_cap_access(cap: CapReceive) -> ExternResult<()> {
    create_cap_claim(CapClaimEntry::new(
        "p2p_message".into(),
        cap.from_agent,
        cap.cap_secret,
    ))?;
    Ok(())
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct GrantCapAccess {
    pub function: String,
    pub agent: AgentPubKey
}

#[hdk_extern]
pub fn create_assigned_cap_access(access: GrantCapAccess) -> ExternResult<GrantCapAccess> {
    //Create function map of cap grant
    let mut functions: GrantedFunctions = HashSet::new();
    let this_zome = zome_info()?.zome_name;
    functions.insert((this_zome.clone(), "list_health_data".into()));

    //Create the cap grant and commit for current agent
    let cap_secret = generate_cap_secret()?;
    create_cap_grant(CapGrantEntry {
        tag: "cap_grant".into(),
        access: CapAccess::from((cap_secret, access.agent.clone())),
        functions,
    })?;

    //Call the zome of target agent and give them the generated cap secret
    call_remote(
        access.agent.clone(),
        zome_info()?.zome_name,
        "receive_cap_access".into(),
        None,
        CapReceive {
            cap_secret: cap_secret,
            from_agent: agent_info()?.agent_initial_pubkey
        },
    )?;
    Ok(access)
}

#[hdk_extern]
fn list_health_data_remote(cap_for: CapReceive) -> ExternResult<Vec<HealthDataOutput>> {
    let remote_result = call_remote(
        cap_for.from_agent,
        zome_info()?.zome_name,
        "list_health_data".to_string().into(),
        Some(cap_for.cap_secret),
        &(),
    )?;
    // If the call_remote call was successfull, we'll get an Ok variant
    if let ZomeCallResponse::Ok(inner_io) = remote_result {
        // We convert the binary bytes values into our HealthDataOutput
        let health_data: Vec<HealthDataOutput> = inner_io.decode()?;
        Ok(health_data)
    } else {
        // and if it wasn't, I'm just returning a random error I found in the
        // ElementalChat
        // TODO: refactor this! I have no idea if that's the right type here,
        // but it compiles :D
        Err(WasmError::Guest(String::from("bad stuff")))
    }
}
// save call-remote results to var
// print result

#[hdk_extern]
fn call_cap_claim(filter_by_agent: AgentPubKey) -> ExternResult<Vec<HealthDataOutput>> {
    let cap_tokens = get_cap_tokens(filter_by_agent)?;
    let cap_receive = CapReceive { cap_secret: *cap_tokens[0].secret(), from_agent: cap_tokens[0].grantor().clone()};
    list_health_data_remote(cap_receive)
}