import { Orchestrator, Config, InstallAgentsHapps } from "@holochain/tryorama";
import path from "path";

const conductorConfig = Config.gen();

// Construct proper paths for your DNAs
const radhisDNA = path.join(__dirname, "../../workdir/dna/radhis_dna.dna");

// create an InstallAgentsHapps array with your DNAs to tell tryorama what
// to install into the conductor.
const installation: InstallAgentsHapps = [
  // agent 0
  [
    // happ 0
    [radhisDNA],
  ],
];

const sleep = (ms) =>
  new Promise((resolve) => setTimeout(() => resolve(null), ms));

const orchestrator = new Orchestrator();

orchestrator.registerScenario("create health_data entry", async (s, t) => {
  const [alice,bob,carol] = await s.players([conductorConfig,conductorConfig,conductorConfig]);

  // install your happs into the coductors and destructuring the returned happ data using the same
  // array structure as you created in your installation array.
  //const [[alice_common],[bob_common],[carol_common]] = await alice.installAgentsHapps(installation);
  const [[alice_common]] = await alice.installAgentsHapps(installation);
  const [[bob_common]] = await bob.installAgentsHapps(installation);
  const [[carol_common]] = await bob.installAgentsHapps(installation);

  await s.shareAllNodes([alice, bob, carol])

  // remember the zome is renaming to camelCase
  // cells.call expects zome / function / parameters
  const alice_result_1 = await alice_common.cells[0].call(
    "radhis", 
    "create_health_data", 
    {
      content: "foo",
      resourceType: "bar"
    }
  );
  // test #1 - valid result for Alice's entry
  t.ok(alice_result_1);

  await sleep(500);

  const alice_result_2 = await alice_common.cells[0].call(
    "radhis", 
    "create_health_data", 
    {
      content: "more alice data",
      resourceType: "Respiratory"
    }
  );

  // now we'll make some other entries for fun
  const bob_result_1 = await bob_common.cells[0].call(
    "radhis",
    "create_health_data",
    {
      content: "amazing data",
      resourceType: "Cardiology"
    }
  );

  const carol_result_1 = await carol_common.cells[0].call(
    "radhis",
    "create_health_data",
    {
      content: "carol health data!",
      resourceType: "Orthopaedics"
    }
  );

  // DHT STATE. Alice has 2 entries; Bob and Carol have 1 each

  // we really need to sleep here - less than this is unreliable for entries to propogate and retrieve
  await sleep(2500);

  // gets a vector of all health data entries in the DHT
  const list_health_data_1 = await alice_common.cells[0].call(
    "radhis",
    "list_health_data",
    null
  );

  // we made a total of 4 entries above, so we should get 4 back here!
  console.log(list_health_data_1);
  // test #2 - can we get all 4 entries back?
  t.equal(list_health_data_1.length, 4);

  // start another test for Carol to retrieve Bob's entry
  // pass entry hash of required entry to variable
  let bob1_entry_hash = bob_result_1.entryHash;

  // now pass it to get_health_data_entry function
  const get_entry_1 = await carol_common.cells[0].call(
    "radhis",
    "get_health_data_entry",
    bob1_entry_hash
  )

  // test #3 - did we get a valid entry?
  t.ok(get_entry_1);
  // test #4 - did we get the correct entry?
  t.equal(get_entry_1.content, 'amazing data');


  // now do some cap grant stuff
  // Bob is giving Alice capGrant to access list_health_data
  let capAssignedGrant = await bob_common.cells[0].call(
    "radhis",
    "create_assigned_cap_access",
    {
      function: "list_health_data",
      agent: alice_common.agent,
    }
  );
  console.log("PRINT from create_assigned_cap_access:")
  console.log(capAssignedGrant);
  // test #5 - do we have a valid cap grant?
  t.ok(capAssignedGrant);

  console.log("Getting cap tokens for alice");
  let capAccess2 = await alice_common.cells[0].call(
    "radhis",
    "get_cap_tokens",
    bob_common.agent
  );
  console.log("PRINT from get_cap_tokens:")
  console.log(capAccess2);
  // test #6 - do we have valid cap token?
  t.ok(capAccess2);
  // test #7
  t.deepEqual(capAccess2.length, 1);

  let TokenResults = await alice_common.cells[0].call(
    "radhis",
    "call_cap_claim",
    bob_common.agent
  );
  console.log(TokenResults);
  t.ok(TokenResults)

});

orchestrator.run();