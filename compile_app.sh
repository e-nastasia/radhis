#!/bin/bash

# CAUTION: this file is horrible, it does no checking for presence of workdir to compile dna and happ.
# Before use, you should have done the following:
# 1. compiled WASM
# 2. created dna dir with `hc dna init workdir/dna`
# 3. edited the dna.yaml file created to reference the WASM created in step 1
# 4. created DNA bundle with `hc dna pack workdir/dna`
# 5. created happ dir with `hc app init workdir/happ`
# 6. edited the happ.yaml file created to reference the DNA bundle created in step 4
# 7. created Happ bundle with `hc app pack workdir/happ`
# Refer to: https://github.com/holochain/holochain-dna-build-tutorial for more info.

cd zome/zomes/radhis
CARGO_TARGET=target cargo build --release --target wasm32-unknown-unknown
cd ../../..
hc dna pack workdir/dna
hc app pack workdir/happ
