use crate::utils;
use hc_utils::{WrappedAgentPubKey, WrappedEntryHash};
use hdk::prelude::*;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum EventLocation {
    Resource(WrappedEntryHash),
    Custom(String),
}

#[hdk_entry(id = "health_data", visibility = "public")]
#[serde(rename_all = "camelCase")]
#[derive(Clone)]
pub struct HealthData {
    pub created_by: WrappedAgentPubKey,
    pub content: String,
    pub resource_type: String,
}

#[hdk_entry(id = "audit_data", visibility = "public")]
#[serde(rename_all = "camelCase")]
#[derive(Clone)]
pub struct AuditData {
    pub created_by: WrappedAgentPubKey,
    pub record_header: String,
    pub resource_type: String,
}

#[derive(Clone, Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct HealthDataInput {
   pub content: String,
   pub resource_type: String,
}

#[derive(Clone, Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct HealthDataOutput {
   entry_hash: WrappedEntryHash,
   entry: HealthData,
}

#[derive(Clone, Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct AuditDataInput {
   pub record_header: String,
   pub resource_type: String,
}

#[derive(Clone, Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct AuditDataOutput {
   entry_hash: WrappedEntryHash,
   entry: AuditData,
}

// Creates a new health_data entry
pub fn create_health_data(
    health_data_input: HealthDataInput,
) -> ExternResult<HealthDataOutput> {
    let agent_info = agent_info()?;
 
    let health_data = HealthData {
        created_by: WrappedAgentPubKey(agent_info.agent_latest_pubkey.clone()),
        content: health_data_input.content,
        resource_type: health_data_input.resource_type
    };
 
    create_entry(&health_data)?;
 
    let health_data_hash = hash_entry(&health_data)?;
 
    let path = health_data_path();
 
    path.ensure()?;
 
    create_link(path.hash()?, health_data_hash.clone(), ())?;
 
    Ok( HealthDataOutput{
        entry_hash: WrappedEntryHash(health_data_hash),
        entry: health_data,
    })
}

// Creates a new audit_data entry
pub fn create_audit_data(
    audit_data_input: AuditDataInput,
) -> ExternResult<AuditDataOutput> {
    let agent_info = agent_info()?;
 
    let audit_data = AuditData {
        created_by: WrappedAgentPubKey(agent_info.agent_latest_pubkey.clone()),
 
        record_header: audit_data_input.record_header,
        resource_type: audit_data_input.resource_type
    };
 
    create_entry(&audit_data)?;
 
    let audit_data_hash = hash_entry(&audit_data)?;
 
    let path = audit_data_path();
 
    path.ensure()?;
 
    create_link(path.hash()?, audit_data_hash.clone(), ())?;
 
    Ok( AuditDataOutput{
        entry_hash: WrappedEntryHash(audit_data_hash),
        entry: audit_data,
    })
}

// returns all health_data entries (?)
pub fn list_health_data() -> ExternResult<Vec<HealthDataOutput>> {
    let path = health_data_path();

    let links = get_links(path.hash()?, None)?;

    links
        .into_inner()
        .iter()
        .map(|link| {
            utils::try_get_and_convert::<HealthData>(link.target.clone())
                .map(|(entry_hash, entry)| HealthDataOutput { entry_hash, entry })
        })
        .collect()
}

// returns audit_data entries
pub fn get_audit_data() -> ExternResult<Vec<AuditDataOutput>> {
    let path = audit_data_path();

    let links = get_links(path.hash()?, None)?;

    links
        .into_inner()
        .iter()
        .map(|link| {
            utils::try_get_and_convert::<AuditData>(link.target.clone())
                .map(|(entry_hash, entry)| AuditDataOutput { entry_hash, entry })
        })
        .collect()
}

// this might work as a function to get a specific health_data entry
#[hdk_extern]
pub fn get_health_data_entry(health_data_hash: WrappedEntryHash) -> ExternResult<HealthData> {
    utils::try_get_and_convert::<HealthData>(health_data_hash.0).map(|(_, entry)| entry)
}

// Private helpers - anchors for the path
fn health_data_path() -> Path {
    Path::from(format!("health_data"))
}

fn audit_data_path() -> Path {
    Path::from(format!("audit_data"))
}
